@extends('plantilla')
@section('contenido')
<div class="row mt-3">
    <div class="col-md-6 offset-md-3">
        <div class="card">
            <div class="card-header bg-dark text-white">Editar Tipo</div>
            <div class="card-body">
                <form id="frmEstados" method="POST" action="{{url("tipos",[$tipo])}}">
                    @csrf
                    @method('PUT')
                    <div class="input-group mb-3">
                        <span class="input-group-text"><i class="fa-solid fa-cloud-showers-water"></i></span>
                        <input type="text" name="nombre" value="{{ $tipo->nombre}}" class="form-control" maxlength="50" placeholder="Nombre" required>
                    </div>
                    <div class="input-group mb-3">
                        <span class="input-group-text"><i class="fa-solid fa-triangle-exclamation"></i></span>
                        <input type="text" name="riesgo" value="{{ $tipo->riesgo}}" class="form-control" maxlength="50" placeholder="Riesgo" required>
                    </div>
                    <div class="input-group mb-3">
                        <span class="input-group-text"><i class="fa-regular fa-calendar-days"></i></span>
                        <input type="text" name="fecha" value="{{ $tipo->fecha}}" class="form-control" maxlength="50" placeholder="Fecha" required>
                    </div>
                    <div class="input-group mb-3">
                        <span class="input-group-text"><i class="fa-regular fa-clock"></i></span>
                        <input type="text" name="categoria" value="{{ $tipo->categoria}}" class="form-control" maxlength="50" placeholder="Duracion" required>
                    </div>
                    <div class="input-group mb-3">
                        <span class="input-group-text"><i class="fa-solid fa-user-injured"></i></span>
                        <input type="text" name="fallecimientos" value="{{ $tipo->fallecimientos}}" class="form-control" maxlength="50" placeholder="Damnificados" required>
                    </div>
                    <div class="input-group mb-3">
                        <span class="input-group-text"><i class="fa-solid fa-globe"></i></span>
                        <select name="id_estado" class="form-select" required>
                            <option value="">Estado</option>
                            @foreach($estados as $row)
                                @if ($row->id == $tipo->id_estado)
                                    <option selected value="{{ $row->id}}">{{ $row->estado}}</option>
                                @else
                                    <option value="{{ $row->id}}">{{ $row->estado}}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                    <div class="d-grid col-6 mx-auto">
                        <button class="btn btn-success"><i class="fa-solid fa-floppy-disk"></i>  Guardar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection