@extends('plantilla')
@section('contenido')
<div class="row mt-3">
    <div class="col-md-4 offset-md-4">
        <div class="d-grid mx-auto">
            <button class="btn btn-dark" data-bs-toggle="modal" data-bs-target="#modalEstados">
                <i class="fa-regular fa-circle-up"></i> Añadir
            </button>
        </div>
    </div>
</div>
<div class="row mt-3">
    <div class="col-12 col-lg-8 offset-0 offset-lg1-2">
        <div class="table-responsive">
            <table class="table table-bordered table-hover">
                <thead><tr><th>#</th><th>Nombre</th><th>Tipo</th><th>Fecha</th><th>Duracion</th><th>Damnificados</th><th>Estado</th><th>Editar</th><th>Eliminar</th></tr></thead>
                <tbody class="table-group-divider">
                    @php $i=1; @endphp
                    @foreach ($tipos as $row)
                        <tr>
                            <td>{{ $i++ }}</td>
                            <td>{{ $row->nombre }}</td>
                            <td>{{ $row->riesgo }}</td>
                            <td>{{ $row->fecha }}</td>
                            <td>{{ $row->categoria }}</td>
                            <td>{{ $row->fallecimientos }}</td>
                            <td>{{ $row->estado }}</td>
                            <td>
                                <a href="{{ url('tipos',[$row]) }}" class="btn btn-warning"><i class="fa-solid fa-edit"></i></a>
                            </td>
                            <td>
                                <form method="POST" action="{{ url('tipos',[$row]) }}">
                                @method("delete")
                                @csrf
                                <button class="btn btn-danger"><i class="fa-solid fa-trash"></i></button>
                            </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="modal fade" id="modalEstados" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <label class="h5" id="titulo_modal">Añadir tipos</label>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form id="frmEstados" method="POST" action="{{url("tipos")}}">
                    @csrf
                    <div class="input-group mb-3">
                        <span class="input-group-text"><i class="fa-solid fa-cloud-showers-water"></i></span>
                        <input type="text" name="nombre" class="form-control" maxlength="50" placeholder="Nombre" required>
                    </div>
                    <div class="input-group mb-3">
                        <span class="input-group-text"><i class="fa-solid fa-triangle-exclamation"></i></span>
                        <input type="text" name="riesgo" class="form-control" maxlength="50" placeholder="Riesgo" required>
                    </div>
                    <div class="input-group mb-3">
                        <span class="input-group-text"><i class="fa-regular fa-calendar-days"></i></span>
                        <input type="text" name="fecha" class="form-control" maxlength="50" placeholder="Fecha" required>
                    </div>
                    <div class="input-group mb-3">
                        <span class="input-group-text"><i class="fa-regular fa-clock"></i></span>
                        <input type="text" name="categoria" class="form-control" maxlength="50" placeholder="Duracion" required>
                    </div>
                    <div class="input-group mb-3">
                        <span class="input-group-text"><i class="fa-solid fa-user-injured"></i></span>
                        <input type="text" name="fallecimientos" class="form-control" maxlength="50" placeholder="Damnificados" required>
                    </div>
                    <div class="input-group mb-3">
                        <span class="input-group-text"><i class="fa-solid fa-globe"></i></span>
                        <select name="id_estado" class="form-select" required>
                            <option value="">Estado</option>
                            @foreach($estados as $row)
                            <option value="{{ $row->id}}">{{ $row->estado}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="d-grid col-6 mx-auto">
                        <button class="btn btn-success"><i class="fa-solid fa-floppy-disk"></i>  Guardar</button>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnCerrar" class="btn btn-secondary" data-bs-dismiss="modal">Cerar</button>
            </div>
        </div>
    </div>
</div>
@endsection