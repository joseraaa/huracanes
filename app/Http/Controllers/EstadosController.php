<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Estados;

class EstadosController extends Controller
{
    public function index()
    {
        $estados = Estados::all();
        return view('estados',compact('estados'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $estados = new Estados($request->input());
        $estados->saveOrFail();
        return redirect('estados');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $estado = Estados::find($id);
        return view('editEstado',compact('estado'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $estado = Estados::find($id);
        $estado->fill($request->input())->saveOrFail();
        return redirect('estados');
    }

    public function destroy($id)
    {
        $estado = Estados::find($id);
        $estado->delete();
        return redirect('estados');
    }
}
