<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Tipos;
use App\Models\Estados;

class TiposController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $tipos = Tipos::select('tipos.id','nombre','riesgo','fecha','categoria','fallecimientos','id_estados','estados')->join('estados','estados.id','=','tipos.id_estados')->get();
        $estados = Estados::all();
        return view('tipos',compact('tipos','estados'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $tipo = new Tipos($request->input());
        $tipo->saveOrFail();
        return redirect('tipos');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $tipo = Tipos::find($id);
        $estados =Estados::all();
        return view('editTipo',compact('tipo','estados'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $tipo = Tipos::find($id);
        $tipo->fill($request->input())->saveOrFail();
        return redirect('tipos');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $tipo = Tipos::find($id);
        $tipo->delete();
        return redirect('tipos');
    }
}
